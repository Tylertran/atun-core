# Atun Core

Core logic of Atun Facebook scaper and PDF maker.
This libary can be further separated to
atun-scaper and atun-pdf when necessary.

This is a tool to create beautiful PDF files
from stories and notes at Facebook.
The PDF files can be printed to paper books,
for displaying in living rooms, giving to children etc.

## Development notes

The program is designed as a pipeline of several steps,
so that it's easier to develop each step,
without having to download data from Facebook again and again.

We may use Facebook's GraphQL API, but we use Google's
[Puppeteer](https://github.com/puppeteer/puppeteer)
as an excersise/learn how to use it to scrape websites.

### Debug

To display browser UI:

```
export ATUN_NO_HEADLESS=true
```

To keep browser UI after processing:

```
export ATUN_KEEP_BROWSER=true
```
