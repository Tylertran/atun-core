import fs from 'fs'

export function storeJson(filePath: string, data: any) {
  fs.writeFileSync(filePath, JSON.stringify(data, null, 2))
}

// Returns null if file is not found or invalid JSON.
export function loadJson(filePath: string): any {
  if (!fs.existsSync(filePath)) {
    return null
  }

  try {
    const buffer = fs.readFileSync(filePath)
    return JSON.parse(buffer.toString())
  } catch (e) {
    console.log(e)
    return null
  }
}

module.exports = {
  storeJson,
  loadJson
}
