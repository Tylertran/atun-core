import {Page} from 'puppeteer-core'

import {fetchBodies} from '../bodies'
import {PageMaker} from '../browser'
import {Link} from '../links'

async function getNoteBodyFromPage(page: Page, link: Link) {
  const body = await page.$eval('div[data-sigil="notes-root"]', (container) =>
    container.outerHTML
  )

  return body.replace(`<p>${link.title}</p>`, '')
}

export function fetchNoteBodies(
  pageMaker: PageMaker,
  links: Link[],
  cancelRequested = () => false
) {
  return fetchBodies(pageMaker, links, getNoteBodyFromPage, cancelRequested)
}
