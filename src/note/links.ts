import {Page} from 'puppeteer-core'

import {BASE_URL_PC, DEVICE_PC, PageMaker} from '../browser'
import {fetchLinks, Link} from '../links'

const LINKS_SELECTOR = 'a[href^="https://www.facebook.com/note"]'

async function getNoteLinksWithoutScrolling(
  page: Page,
  toBeRemovedMarkerClassName: string
): Promise<Link[]> {
  return await page.$$eval(LINKS_SELECTOR, (anchors, toBeRemovedMarkerClassName, BASE_URL_PC) => {
    return anchors.flatMap(elem => {
      if (elem.classList.contains(toBeRemovedMarkerClassName)) return []

      const anchor = elem as HTMLAnchorElement
      const title = anchor.innerText
      if (title.length === 0) {
        return []
      }

      const dateContainer = anchor.nextSibling as HTMLDivElement
      const date = dateContainer.innerText

      const url = anchor.href.substring(BASE_URL_PC.length)

      return [{url, title, date}]
    })
  }, toBeRemovedMarkerClassName, BASE_URL_PC)
}

export function fetchNoteLinks(
  pageMaker: PageMaker,
  path: string,
  cancelRequested: () => boolean
) {
  return fetchLinks(
    pageMaker,
    DEVICE_PC,
    path,
    getNoteLinksWithoutScrolling,
    LINKS_SELECTOR,
    cancelRequested
  )
}
