import puppeteer, {Browser, Page, devices} from 'puppeteer-core'
import {loadJson, storeJson} from './util'

export const DEVICE_IPAD = puppeteer.devices['iPad']
export const DEVICE_PC = null

export const BASE_URL_IPAD = 'https://m.facebook.com'
export const BASE_URL_PC = 'https://www.facebook.com'

const BROWSER_PATH = process.env['ATUN_BROWSER_PATH']
const NO_HEADLESS = process.env['ATUN_NO_HEADLESS']
const KEEP_BROWSER = process.env['ATUN_KEEP_BROWSER']

export type LaunchBrowserOptions = {
  headless?: boolean
  handleSIGINT?: boolean
}

export type PageMaker = (url?: string) => Promise<Page>

// https://github.com/puppeteer/puppeteer/blob/main/docs/api.md#puppeteerlaunchoptions
export async function launchBrowser(
  doWithBrowser: (browser: Browser) => Promise<void>,
  cookiesFilePath: string,
  options: LaunchBrowserOptions = {headless: true}
) {
  const effectiveHeadless = options.headless && NO_HEADLESS !== 'true'
  const browser = await puppeteer.launch({
    ...options,
    executablePath: BROWSER_PATH,
    headless: effectiveHeadless,
    defaultViewport: null
  })

  await restoreCookies(cookiesFilePath, browser)

  try {
    await doWithBrowser(browser)
  } catch (e) {
    console.log(e)
  }

  if (effectiveHeadless || KEEP_BROWSER !== 'true') {
    await browser.close()
  }
}

// Electron needs special page maker:
// https://github.com/electron/electron/issues/17776
// https://github.com/TrevorSundberg/puppeteer-in-electron
export async function newPage(
  pageMaker: PageMaker,
  device: devices.Device | null,
  onlyLoadContent: boolean,
  url?: string
) {
  const page = await pageMaker(url)

  if (device !== DEVICE_PC) {
    await page.emulate(device)
  }

  // https://www.scrapehero.com/how-to-increase-web-scraping-speed-using-puppeteer/
  if (onlyLoadContent) {
    await page.setRequestInterception(true)

    page.on('request', (req) => {
      const rt = req.resourceType()
      // Keep 'stylesheet' because getting Facebook links doesn't work well without CSS
      if (rt == 'font' || rt == 'image') {
        req.abort()
      } else {
        req.continue()
      }
    })
  }

  if (url) {
    await page.goto(url, {waitUntil : 'networkidle0'})
  }

  return page
}

export async function newFbPage(
  pageMaker: PageMaker,
  device: devices.Device | null,
  onlyLoadContent: boolean,
  relUrl: string
) {
  const baseUrl = device === DEVICE_PC ? BASE_URL_PC : BASE_URL_IPAD
  const url = `${baseUrl}${relUrl}`
  return await newPage(pageMaker, device, onlyLoadContent, url)
}

export function newPageMaker(browser: Browser): PageMaker {
  return async (url?: string) => {
    const page = await browser.newPage()

    // Avoid the annoying permission prompt,
    // asking for permisions like geolocation, notifications etc.;
    // permisions not in the list will automatically be denied
    if (url) {
      const context = browser.defaultBrowserContext()
      context.overridePermissions(url, [])
    }

    return page
  }
}

export async function storeCookies(filePath: string, page: Page) {
  const cookies = await page.cookies()
  storeJson(filePath, cookies)
  console.log(`Cookies stored to ${filePath}`)
}

async function restoreCookies(filePath: string, browser: Browser) {
  const cookies = loadJson(filePath)
  if (cookies) {
    const page = await browser.newPage()
    await page.setCookie(...cookies)
    page.close()
    console.log(`Cookies loaded from ${filePath}`)
  }
}
