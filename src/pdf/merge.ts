import {loadPdf, savePdf} from './pdf'

export async function mergePdfs(inFiles: string[], outFile: string) {
  const finalPdf = await loadPdf(inFiles[0])

  let fileIdx = 1
  while (fileIdx < inFiles.length) {
    const pdf = await loadPdf(inFiles[fileIdx])
    const pageCount = pdf.getPageCount()

    let pageIdx = 0
    while (pageIdx < pageCount) {
      const [page] = await finalPdf.copyPages(pdf, [pageIdx])
      finalPdf.addPage(page)
      pageIdx++
    }

    fileIdx++
  }

  await savePdf(outFile, finalPdf)
}
