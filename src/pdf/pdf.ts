// https://github.com/Hopding/pdf-lib seems better.
//
// pdf-merger-js uses a lot of memory:
// https://github.com/nbesli/pdf-merger-js/issues/27
// and produces PDF bigger than expected:
// https://github.com/nbesli/pdf-merger-js/issues/31

import fs from 'fs'
import {PDFDocument} from 'pdf-lib'

export async function loadPdf(filePath: string): Promise<PDFDocument> {
  const bytes = fs.readFileSync(filePath)
  return await PDFDocument.load(bytes)
}

export async function savePdf(filePath: string, doc: PDFDocument) {
  const bytes = await doc.save()
  fs.writeFileSync(filePath, bytes)
}
