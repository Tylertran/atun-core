import {Page, PDFFormat} from 'puppeteer-core'
import {loadPdf, savePdf} from './pdf'

// https://github.com/puppeteer/puppeteer/blob/main/docs/api.md#pagepdfoptions
const PAGE_MARGIN = 50

// https://github.com/puppeteer/puppeteer/issues/1822
const PAGE_FOOTER = `
  <div style="color: lightgray; font-size: 10px; padding-top: 5px; text-align: center; width: 100%;">
    <span class="pageNumber"></span>
  </div>`

// https://github.com/puppeteer/puppeteer/issues/3383
async function prependEmptyPages(page: Page, numPages: number) {
  await page.addStyleTag({content: `
    .empty-print-page { page-break-after: always; visibility: hidden; }
  `})

  await page.evaluate((numPages) => {
    for (let count = 1; count <= numPages; count++) {
      const emptyPage = document.createElement('div')
      emptyPage.className = 'empty-print-page'
      emptyPage.textContent = 'must be set'
      document.body.prepend(emptyPage)
    }
  }, numPages)
}

async function removeEmptyPages(filePath: string, numPages: number) {
  const doc = await loadPdf(filePath)

  for (let count = 1; count <= numPages; count++) {
    doc.removePage(0)
  }

  await savePdf(filePath, doc)
}

// Cannot generate PDF in non-headless mode:
// https://github.com/puppeteer/puppeteer/issues/1829
export async function printToPdf(
  page: Page,
  format: PDFFormat,
  path: string,
  withMargin: boolean,
  withPageNumber: boolean,
  startPage = 1
) {
  const margin = withMargin ? PAGE_MARGIN : 0

  if (withPageNumber && startPage > 1) {
    await prependEmptyPages(page, startPage - 1)
  }

  await page.pdf({
    format,
    path,
    margin: {top: margin, bottom: margin, left: margin, right: margin},
    printBackground: true,
    displayHeaderFooter: withPageNumber,
    headerTemplate: '<span></span>',
    footerTemplate: PAGE_FOOTER
  })

  if (withPageNumber && startPage > 1) {
    await removeEmptyPages(path, startPage - 1)
  }

  console.log(`Printed to ${path}`)
}
