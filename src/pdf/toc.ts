import {PDFDocument, rgb} from 'pdf-lib'
import {loadPdf, savePdf} from './pdf'

async function drawPageNumber(filePath: string, doc: PDFDocument, pageNumber: number) {
  const pages = doc.getPages()

  for (let idx = 0; idx < pages.length; idx++) {
    const page = pages[idx]
    const {width, height} = page.getSize()
    const num = pageNumber + idx

    page.drawText(num.toString(), {
      x: width / 2,
      y: height * 0.03,
      size: height * 0.01,
      color: rgb(0.8, 0.8, 0.8)
    })
  }

  await savePdf(filePath, doc)
}

/**
 * Returns page numbers of articles, starting from page 1.
 */
export async function getToc(
  articlePdfPaths: string[],
  drawPageNumbers: boolean
): Promise<number[]> {
  const toc = [1]

  for (const pdfPath of articlePdfPaths) {
    const doc = await loadPdf(pdfPath)
    const currentPageNumber = toc[toc.length - 1]
    const nextPageNumber = currentPageNumber + doc.getPageCount()
    toc.push(nextPageNumber)

    if (drawPageNumbers) {
      drawPageNumber(pdfPath, doc, currentPageNumber)
    }
  }

  toc.pop()
  return toc
}
