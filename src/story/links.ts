import {Page} from 'puppeteer-core'

import {DEVICE_IPAD, PageMaker} from '../browser'
import {fetchLinks, Link, TitleOptions} from '../links'

async function getStoryLinksWithoutScrolling(
  page: Page,
  toBeRemovedMarkerClassName: string,
  titleOptions?: TitleOptions
): Promise<Link[]> {
  return await page.$$eval('.story_body_container', (containers, toBeRemovedMarkerClassName, titleOptions) => {
    // Title should not be too long
    const isTitleShortEnough = (title: string) => {
      const maxChars = titleOptions?.maxChars || -1
      return maxChars < 0 ? true : title.length <= maxChars
    }

    // Title should contain most upper case characters
    // CHUI BAO vs CHUI HÁNG
    // TÔi SỐNG ẢO
    const isTitleMostlyUpperCase = (title: string) => {
      const upperCaseRatio = titleOptions?.upperCaseRatio || -1
      if (upperCaseRatio < 0) {
        return true
      }

      const chars = Array.from(title)
      const realChars = chars.filter((char) => !/[\s0-9\.\!\?"']/.test(char))
      const numNonUpperCases = realChars.reduce((sum, char) => char !== char.toUpperCase() ? sum + 1 : sum, 0)
      return numNonUpperCases < realChars.length * upperCaseRatio
    }

    const isTitleValid = (title: string) => {
      return isTitleShortEnough(title) && isTitleMostlyUpperCase(title) ? title : null
    }

    const getTitle = (container: Element) => {
      const ps = Array.from(container.querySelectorAll('p'))
      if (ps.length < 2) return null

      // If we use textContent (instead of innerHTML), "a &amp; b"
      // will be automatically converted to "a & b".
      // It will not be compatible with outerHTML at getStoryBody.
      const cs = ps.map(p => p.innerHTML)
      const title = cs[0]

      return isTitleValid(title) ? title : null
    }

    const getDate = (container: any) => {
      return container.querySelector('abbr').textContent
    }

    // Only collect links to stories in the diary format
    return containers.flatMap(container => {
      if (container.classList.contains(toBeRemovedMarkerClassName)) return []

      const sa = container.querySelector('a[href^="/story"]')
      const ga = container.querySelector('a[href^="/groups/"]')
      if (!sa && !ga) return []

      const url = sa ? sa.getAttribute('href')! : ga!.getAttribute('href')!

      const title = getTitle(container)
      if (!title) return []

      const date = getDate(container)
      return [{url, title, date}]
    })
  }, toBeRemovedMarkerClassName, titleOptions)
}

export function fetchStoryLinks(
  pageMaker: PageMaker,
  path: string,
  cancelRequested: () => boolean,
  titleOptions?: TitleOptions
) {
  return fetchLinks(
    pageMaker,
    DEVICE_IPAD,
    path,
    getStoryLinksWithoutScrolling,
    'article',
    cancelRequested,
    titleOptions
  )
}
