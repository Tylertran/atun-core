import {Page} from 'puppeteer-core'

import {fetchBodies} from '../bodies'
import {PageMaker} from '../browser'
import {Link} from '../links'

async function getStoryBodyFromPage(page: Page, link: Link) {
  const body = await page.$eval('.story_body_container', (container) => {
    // Remove header to save disk space for body,
    // because we won't use it in the diary anyway
    container.querySelector('header')!.remove()
    return container.outerHTML
  })

  return body.replace(`<p>${link.title}</p>`, '')
}

export function fetchStoryBodies(
  pageMaker: PageMaker,
  links: Link[],
  cancelRequested = () => false
) {
  return fetchBodies(pageMaker, links, getStoryBodyFromPage, cancelRequested)
}
