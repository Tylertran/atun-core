import {devices, Page} from 'puppeteer-core'
import {PageMaker, newFbPage} from './browser'

// Mark stories to be removed from the page to save memory
const TO_BE_REMOVED_MARKER = 'TO_BE_REMOVED_BY_ATUN'
const RENANTS_LIMIT = 10

const CONTINUOUS_NO_MORE_STORIES_LIMIT = 2

export type Link = {
  url: string
  title: string
  date: string
}

export type TitleOptions = {
  /** Set to negative number to disable. */
  maxChars: number

  /** Set to negative number to disable. */
  upperCaseRatio: number
}

// https://intoli.com/blog/scrape-infinite-scroll/
async function scrollDownToLoadMore(page: Page) {
  const previousHeight = await page.evaluate('document.body.scrollHeight')
  await page.evaluate('window.scrollTo(0, document.body.scrollHeight)')

  try {
    await page.waitForFunction(`document.body.scrollHeight > ${previousHeight}`)
  } catch {
  }

  // Wait a little more, just in case (and to avoid rate limiting if any)
  // await page.waitFor(1000)
}

async function countLinks(page: Page, selector: string): Promise<number> {
  return await page.$$eval(selector, elems => elems.length)
}

async function markLinksToBeRemoved(page: Page, selector: string, toBeRemovedMarkerClassName: string): Promise<void> {
  await page.$$eval(selector, (elems, toBeRemovedMarkerClassName) => {
    elems.forEach(elem => elem.classList.add(toBeRemovedMarkerClassName))
  }, toBeRemovedMarkerClassName)
}

// Newly collected links will be yielded in batch for speed, instead of one by one.
export async function* fetchLinks(
  pageMaker: PageMaker,
  device: devices.Device | null,
  path: string,
  getLinksWithoutScrolling: (page: Page, toBeRemovedMarkerClassName: string, titleOptions?: TitleOptions) => Promise<Link[]>,
  linksSelector: string,
  cancelRequested: () => boolean,
  titleOptions?: TitleOptions
) {
  const page = await newFbPage(pageMaker, device, true, path)

  let haveMoreStories = true
  let continuousNoMoreStoriesCount = 0

  while ((haveMoreStories || continuousNoMoreStoriesCount < CONTINUOUS_NO_MORE_STORIES_LIMIT) && !cancelRequested()) {
    if (cancelRequested()) return

    const links = await getLinksWithoutScrolling(page, TO_BE_REMOVED_MARKER, titleOptions)

    // Set class TO_BE_REMOVED_MARKER
    await markLinksToBeRemoved(page, linksSelector, TO_BE_REMOVED_MARKER)

    if (links.length > 0) yield links

    // See comment of TO_BE_REMOVED_MARKER
    const numArticlesBefore = await countLinks(page, linksSelector)
    await scrollDownToLoadMore(page)

    const numArticlesAfter = await countLinks(page, linksSelector)
    haveMoreStories = numArticlesAfter > numArticlesBefore

    if (haveMoreStories) {
      continuousNoMoreStoriesCount = 0

      // Actually remove (see above)
      await page.$$eval(`.${TO_BE_REMOVED_MARKER}`, (elems, remnantsLimit) => {
        const numToRemove = elems.length - remnantsLimit
        if (numToRemove > 0) {
          elems.slice(0, numToRemove).forEach(e => e.remove())
        }
      }, RENANTS_LIMIT)
    } else {
      continuousNoMoreStoriesCount++
    }
  }
}
