import {Page} from 'puppeteer-core'

import {DEVICE_IPAD, PageMaker, newFbPage} from './browser'
import {Link} from './links'

async function getBodyFromLink(pageMaker: PageMaker, getBodyFromPage: (page: Page, link: Link) => Promise<string>, link: Link) {
  const {url} = link
  const page = await newFbPage(pageMaker, DEVICE_IPAD, true, url)

  try {
    return await getBodyFromPage(page, link)
  } finally {
    // Close to save memory
    page.close()
  }
}

export type FetchedBody = {idx: number} & ({body: string} | {error: Error})

// Get one by one to avoid rate limiting if any.
export async function* fetchBodies(
  pageMaker: PageMaker,
  links: Link[],
  getBodyFromPage: (page: Page, link: Link) => Promise<string>,
  cancelRequested = () => false
) {
  for (let idx = 0; idx < links.length; idx++) {
    if (cancelRequested()) return

    const link = links[idx]
    try {
      const body = await getBodyFromLink(pageMaker, getBodyFromPage, link)
      yield {idx, body} as FetchedBody
    } catch (error) {
      yield {idx, error} as FetchedBody
    }
  }
}
