import {Page, PDFFormat} from 'puppeteer'

import {PageMaker} from '../browser'
import {Article, DEVICE_IPAD, newFbPage, printToPdf} from '../index'
import {loadPdf} from '../pdf/pdf'

/**
 * Makes printing prettier.
 */
async function injectPrintStyleTag(page: Page) {
  await page.addStyleTag({content: `
    body {
      /* Default is gray rgb(218, 221, 225) */
      background-color: white !important;

      font-size: 150% !important;
      line-height: 150% !important;
    }

    header {
      text-align: center !important;
    }

    abbr {
      color: gray !important;
      font-size: 90% !important;
    }

    .story_body_container {
      page-break-after: always !important;
    }
  `})
}

export function getArticleHtml(article: Article): string {
  const {title, date, body} = article
  let html = ''
  html += `<header><h2>${title}</h2>\n`
  html += `<abbr>${date}</abbr></header>\n\n`
  html += body
  return html
}

/**
 * Creates a separate file for each article, so that it's easy to get page numbers
 * for generating table of contents.
 */
export async function generateArticlePdfs(
  articles: Article[],
  pageMaker: PageMaker,
  format: PDFFormat,
  onPageReady: (articleIdx: number) => Promise<string | void>
) {
  // Set CSS before HTML
  const page = await newFbPage(pageMaker, DEVICE_IPAD, false, '')
  await injectPrintStyleTag(page)

  // Get HTML template
  let placeholder = 'REPLACE ME ' + Date.now()
  await page.evaluate((placeholder) => {
    const body = document.querySelector('body')!
    body.innerHTML = placeholder
  }, placeholder)
  const template = await page.content()

  let startPage = 1
  for (let idx = 0; idx < articles.length; idx++) {
    const article = articles[idx]
    const articleHtml = getArticleHtml(article)

    // Use `setContent`, as the following trick doesn't work
    // because the image is set via CSS `background-image`:
    // https://stackoverflow.com/questions/46160929/puppeteer-wait-for-all-images-to-load-then-take-screenshot
    const pageHtml = template.replace(placeholder, articleHtml)
    await page.setContent(pageHtml, {waitUntil: 'networkidle0'})

    const pdfPath = await onPageReady(idx)
    if (pdfPath) {
      await printToPdf(page, format, pdfPath, true, true, startPage)
      const doc = await loadPdf(pdfPath)
      startPage += doc.getPageCount()
    }
  }
}
