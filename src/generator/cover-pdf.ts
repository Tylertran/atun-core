import fs from 'fs'
import {PDFFormat} from 'puppeteer-core'

import {DEVICE_IPAD, newPage, PageMaker, printToPdf} from '../index'

export function getImageDataUrl(imagePath: string) {
  const buffer = fs.readFileSync(imagePath)
  const base64 = buffer.toString('base64')

  const extention = imagePath.split('.').pop()
  return `data:image/${extention};base64,${base64}`
}

export async function generateCoverPdf(
  dataUrl: string,
  pageMaker: PageMaker,
  format: PDFFormat,
  pdfPath?: string
) {
  const page = await newPage(pageMaker, DEVICE_IPAD, true)
  await page.setContent(`
    <img src="${dataUrl}" style="width: 100%; page-break-after: right;" />
  `)

  if (pdfPath) {
    await printToPdf(page, format, pdfPath, false, false)
  }
}
