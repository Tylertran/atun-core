import {PDFFormat, Page} from 'puppeteer-core'
import {Article, DEVICE_IPAD, getToc, newFbPage, PageMaker, printToPdf} from '../index'

// Make printing prettier
async function injectPrintStyleTag(page: Page) {
  await page.addStyleTag({content: `
    body {
      /* Default is gray rgb(218, 221, 225) */
      background-color: white !important;

      margin-left: 2em !important;

      font-size: 150% !important;
      line-height: 150% !important;
    }

    .toc {
      page-break-after: right;
    }

    .toc-item {
      margin-bottom: 0.5em;
      page-break-inside: avoid;
    }

    .title {
      display: block;
    }

    .page-number {
      color: gray;
      font-size: 90%;
    }

    .date {
      color: gray;
      font-size: 90%;
    }
  `})
}

export async function generateTocPdf(
  articles: Article[],
  pageMaker: PageMaker,
  format: PDFFormat,
  articlePdfPaths: string[],
  drawPageNumbers: boolean,
  tocPdfPath?: string
) {
  const toc = await getToc(articlePdfPaths, drawPageNumbers)

  const items = articles.map(({title, date}, idx) => {
    const pageNumber = toc[idx]
    return `
      <li class="toc-item">
        <span class="title">${title}</span>
        <span class="page-number">${pageNumber} - </span>
        <span class="date">${date}</span>
      </li>
    `
  })

  const html = `<ol class="toc">${items.join('\n')}</ol>`

  // Set CSS before HTML
  const page = await newFbPage(pageMaker, DEVICE_IPAD, false, '')
  await injectPrintStyleTag(page)

  await page.evaluate((html) => {
    const body = document.querySelector('body')!
    body.innerHTML = html
  }, html)

  if (tocPdfPath) {
    await printToPdf(page, format, tocPdfPath, true, false)
  }
}
