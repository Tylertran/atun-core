export type Article = {
  title: string
  date: string
  body: string
}
